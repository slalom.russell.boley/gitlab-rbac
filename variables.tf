variable "gitlab_admin_token" {
  description = "The admin token to interact with the gitlab api"
}

variable "gitlab_user_list" {
  default = ["slalom.russell.boley", "steven.gale.slalom", "mattstofko"]
}

variable "gitlab_group_path" {
  default = "slalompdx"
}

variable "tfe_team_name" {
  default = "technology-enablement"
}

variable "tfe_org_name" {
  default = "slalom-pdx-te"
}

variable "tfe_user_list" {
  default = ["slalom-russell-boley", "stofkoslalom", "steven-gale-slalom", "slalom-arthur-dayton"]
}
