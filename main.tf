provider "gitlab" {
  token = var.gitlab_admin_token
}

module "gitlab-onboarding" {
  source = "git@gitlab.com:slalompdx/terraform/terraform-gitlab-onboarding.git?ref=v0.0.1"

  gitlab_admin_token  = var.gitlab_admin_token
  gitlab_user_list    = var.gitlab_user_list
  gitlab_group_path   = var.gitlab_group_path
}

provider "tfe" {
}

module "tfe-onboarding" {
  source = "git@gitlab.com:slalompdx/terraform/terraform-tfcloud-onboarding.git"

  tfe_team_name = var.tfe_team_name
  tfe_org_name  = var.tfe_org_name
  tfe_user_list = var.tfe_user_list
}