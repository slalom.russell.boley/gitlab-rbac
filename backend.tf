terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "slalom-pdx-te"

    workspaces {
      name = "gitlab-rbac"
    }
  }
}
