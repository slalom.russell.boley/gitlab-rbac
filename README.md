# gitlab-rbac

## Adding yourself to the team

1. Create an issue describing who you are
1. Create a merge request for that issue you created
1. Open this new branch in the web ide or clone to your local machine
1. Update the `groups.tf` file to have you included as a maintainer of slalom-pdx-te
1. Commit and push the changes to the remote
1. Have your new hire buddy approve the merge request

## Setting up Remote Backend TF Cloud

To connect to terraform cloud and try and run terraform plan you must have a token credential setup to authenticate
with terraform cloud.

To get a token you need to login to terraform cloud and hover over the user icon > user settings > tokens

On the tokens page enter a description for the token and click generate token. copy this you will need to add this to a file below.

This file should look something like this:

```hcl
credentials "app.terraform.io" {
  token = "<TOKEN From Gitlab>"
}
```

for `windows` this file needs to be saved in %APPDATA%\terraform.rc


for `mac` this file should be saved in ~/.terraformrc

## Setting up gitlab token

Next to interface with gitlab you need to have a token set up to populate the `gitlab_admin_token`

One way to populate this is to add a local file `terraform.auto.tfvars` file with content

```hcl-terraform
gitlab_admin_token = "<admin token from gitlab>"
```

After that you should be able to run terraform plan.

# Importing 

When abstracting some base code to another module terraform will enfore 
a destroy and recreate. That can be undesirable since the resource already exists
and you may want to maintain it.

To do that you have to modify the state using `terraform state` commands and `terraform import` commands.
These will let you remove the reference to the old resource name, and import it again as the new resource name.

For example you can run the commands below to migrate from a static resource like before this commit
to a new dynamic format using a for_each loop. Note that the state rm does not delete
the actual resources created, just removes them from the state file.


```bash
# delete an old resource statement declared using resource gitlab_group_membership "i_russell_boley" {}
terraform state rm gitlab_group_membership.i_russell_boley
terraform state rm gitlab_group_membership.i_steven_gale

# Here you can delete an entire module that has been added and all resource from state
terraform state rm module.gitlab-onboarding

# here you can import these resources to exactly where they will be referenced using the dictionary list by username.
# for windows cmd you need to escape the " in the brackets.
terraform import module.gitlab-onboarding.gitlab_group_membership.i[\"steven.gale.slalom\"] 6431070:5220219
terraform import module.gitlab-onboarding.gitlab_group_membership.i[\"slalom.russell.boley\"] 6431070:4757364

```

After Updating the state file you should see no changes when you run terraform plan, even though everything is now declared
through the module.
